package com.example.freddy.dialogo;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class actividad1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.opcionlogin:
                Dialog dialogLogin = new Dialog(actividad1.this);
                dialogLogin.setContentView(R.layout.dlg_login);
                Button botonAutentificar = (Button) dialogLogin.findViewById(R.id.btnAutentificar);
                final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogLogin.findViewById(R.id.txtPassword);
                botonAutentificar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(actividad1.this, cajaUsuario.getText().toString() +"" + cajaClave.getText().toString(),Toast.LENGTH_LONG);
                    }
                });
                dialogLogin.show();
                break;
            case R.id.opcionregistrar:
                break;
        }
        return true;
    }

}
